from django.contrib import admin

# Register your models here.
from CricketMatch.models import User, Team, Player, PlayerHistory, Matches, PointTable

admin.site.register(User)
admin.site.register(Team)
admin.site.register(Player)
admin.site.register(PlayerHistory)
admin.site.register(Matches)
admin.site.register(PointTable)
