from django.conf.urls import url
from . import views
urlpatterns = [
    url(r'^create_team/$', views.CreateTeam.as_view(), name='Create Team'),
    url(r'^all_team/$', views.GetAllTeam.as_view({'get': 'list'}), name='All Team'),
    url(r'^create_player/$', views.CreatePlayer.as_view(), name='Create Player'),
    url(r'^all_player/$', views.GetAllPlayer.as_view({'get': 'list'}), name='All Player'),
    url(r'^team/(?P<team>.+)$', views.GetPlayersByTeam.as_view(), name='All Player By Team'),
    url(r'^play/$', views.PlayCricket.as_view(), name='Play Cricket'),
    url(r'^player/$', views.GetPlayerProfileById.as_view(), name='Player Profile'),
    url(r'^point-table/$', views.GetPointTable.as_view(), name='Player Profile'),
]
