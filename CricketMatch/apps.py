from django.apps import AppConfig


class CricketmatchConfig(AppConfig):
    name = 'CricketMatch'
