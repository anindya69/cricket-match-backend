from django.contrib.auth.models import PermissionsMixin
from django.db import models
from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.utils.translation import ugettext_lazy as _
from django.db.models.aggregates import Count
from random import randint


# Create your models here.
class UserManager(BaseUserManager):

    def create_user(self, email_id):
        """Create and return a `User` with an email, username and password."""

        if email_id is None:
            raise TypeError('Users must have an email address.')

        user = self.model(email_id=self.normalize_email(email_id))

        user.save()

        return user

    def create_superuser(self, email_id, password):
        """
        Create and return a `User` with superuser (admin) permissions.
        """
        if password is None:
            raise TypeError('Superusers must have a password.')

        user = self.create_user(email_id)
        user.is_superuser = True
        user.is_staff = True
        if password is not None:
            user.set_password(password)
        user.save()

        return user


class User(AbstractBaseUser, PermissionsMixin):
    email_id = models.EmailField(_('User ID'), max_length=256, unique=True)
    password = models.CharField(max_length=256)
    is_active = models.BooleanField(_('Is Active'), default=True)
    is_staff = models.BooleanField(_('staff status'), default=False)

    USERNAME_FIELD = 'email_id'
    objects = UserManager()

    def __str__(self):
        return self.email_id

    class Meta:
        db_table = 'user'


class TeamManager(models.Manager):
    def get_query_set(self):
        return super(TeamManager, self).get_query_set().order_by('?')


class Team(models.Model):
    name = models.CharField(_("Team Name"), max_length=254)
    logoUri = models.ImageField(_("Team Logo"), upload_to='images/')
    club_state = models.CharField(_("Club state"), max_length=254)

    objects = models.Manager()  # The default manager.
    randoms = TeamManager()  # The random-specific manager.


class Player(models.Model):
    team = models.ForeignKey(Team, on_delete=models.CASCADE, unique=False)
    firstName = models.CharField(_("First Name"), max_length=254, unique=True)
    lastName = models.CharField(_("Last Name"), max_length=254)
    imageUri = models.ImageField(_("Player Photo"), upload_to='images/player')
    player_jersey_number = models.IntegerField(_("Jersey Number"))
    country = models.CharField(_("Country"), max_length=254)


class PlayerHistory(models.Model):
    player = models.ForeignKey(Player, on_delete=models.CASCADE, related_name='history')
    matches = models.IntegerField(_("Matches"), default=0)
    run = models.IntegerField(_("Total Run"), default=0)
    highest_scores = models.IntegerField(_("Highest Scores"), default=0)
    fifties = models.IntegerField(_("Fifties"), default=0)
    hundreds = models.IntegerField(_("Hundreds"), default=0)


class Matches(models.Model):
    team_1 = models.ForeignKey(Team, null=True, on_delete=models.CASCADE, related_name='team_1')
    team_2 = models.ForeignKey(Team, null=True, on_delete=models.CASCADE, related_name='team_2')
    winner = models.ForeignKey(Team, null=True, on_delete=models.CASCADE, related_name='winner')


class PointTable(models.Model):
    team = models.ForeignKey(Team, unique=False, on_delete=models.CASCADE, related_name='team')
    point = models.IntegerField(_("Point"), default=0)
