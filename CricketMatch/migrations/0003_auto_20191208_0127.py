# Generated by Django 3.0 on 2019-12-07 19:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('CricketMatch', '0002_auto_20191207_1506'),
    ]

    operations = [
        migrations.AlterField(
            model_name='player',
            name='firstName',
            field=models.CharField(max_length=254, unique=True, verbose_name='First Name'),
        ),
    ]
