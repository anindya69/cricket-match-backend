from rest_framework import serializers
from CricketMatch.models import Team, Player, PlayerHistory, Matches, PointTable


class TeamSerializer(serializers.ModelSerializer):
    class Meta:
        model = Team
        fields = '__all__'


class TeamCreateSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=254)
    logoUri = serializers.ImageField()
    club_state = serializers.CharField(max_length=254)

    def create(self, validated_data):
        return Team.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.name = validated_data.get('name', instance.name)
        instance.logoUri = validated_data.get('logoUri', instance.logoUri)
        instance.club_state = validated_data.get('club_state', instance.club_state)
        instance.save()
        return instance


class PlayerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Player
        fields = '__all__'


class PlayerHistorySerializer(serializers.ModelSerializer):
    class Meta:
        model = PlayerHistory
        fields = "__all__"


class PlayerProfileSerializer(serializers.ModelSerializer):
    history = PlayerHistorySerializer(many=True)

    class Meta:
        model = Player
        fields = ('team', 'firstName', 'lastName', 'imageUri', 'player_jersey_number', 'country', 'history')


class PointTableSerializer(serializers.ModelSerializer):
    team = TeamSerializer()

    class Meta:
        model = PointTable
        fields = ('team', 'point')
