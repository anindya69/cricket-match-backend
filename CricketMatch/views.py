import random

from django.views.decorators.csrf import csrf_exempt
from rest_framework import status, viewsets, generics
from rest_framework.response import Response
from rest_framework.views import APIView

# Create your views here.
from .models import Team, Player, PlayerHistory, Matches, PointTable
from .serializers import TeamCreateSerializer, TeamSerializer, PlayerSerializer, PlayerProfileSerializer, PointTableSerializer


def get_team_by_id(team_id):
    try:
        return Team.objects.get(id=team_id)
    except Team.DoesNotExist:
        return None


class CreateTeam(APIView):
    serializer_class = TeamCreateSerializer
    """
    Create Cricket Team
    """

    @csrf_exempt
    def post(self, request):
        """
        :param request:
        :return:
        """
        print(request.data)
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            team = serializer.create(serializer.validated_data)
            PointTable.objects.create(team=team)
            return Response({'status': True, 'message': 'Successfully created'}, status=status.HTTP_201_CREATED)
        else:
            return Response({'status': False, 'message': 'Please enter correct data'}, status=status.HTTP_400_BAD_REQUEST)


class GetAllTeam(viewsets.ModelViewSet):
    queryset = Team.objects.all()
    serializer_class = TeamSerializer
    """
    Get all team details
    """


class CreatePlayer(APIView):
    serializer_class = PlayerSerializer
    """
    Create and Add Player to a team
    """

    @csrf_exempt
    def post(self, request):
        """
        :param request:
        :return:
        """
        team = get_team_by_id(request.data.get('team'))
        if team:
            print(request.data)
            serializer = self.serializer_class(data=request.data)
            if serializer.is_valid():
                player = Player.objects.create(
                    team=team,
                    firstName=serializer.validated_data.get('firstName'),
                    lastName=serializer.validated_data.get('lastName'),
                    imageUri=serializer.validated_data.get('imageUri'),
                    player_jersey_number=serializer.validated_data.get('player_jersey_number'),
                    country=serializer.validated_data.get('country'),
                )
                player.save()
                return Response({'status': True, 'message': 'Successfully created'}, status=status.HTTP_201_CREATED)
            else:
                return Response({'status': False, 'message': 'Please send correct data'}, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({'status': False, 'message': 'Team not found'}, status=status.HTTP_400_BAD_REQUEST)


class GetAllPlayer(viewsets.ModelViewSet):
    queryset = Player.objects.all()
    serializer_class = PlayerSerializer
    """
    Get all player details
    """


class GetPlayersByTeam(generics.ListAPIView):
    serializer_class = PlayerSerializer

    def get_queryset(self):
        """
        This view should return a list of all the players for
        the selected team.
        """
        team = self.kwargs['team']
        return Player.objects.filter(team__id=team)


class GetPlayerProfileById(APIView):
    serializer_class = PlayerProfileSerializer

    def post(self, request):
        """
        This view should return a list of all the players for
        the selected team.
        """
        player = request.data.get('player_id')
        try:
            player_data = Player.objects.get(id=player)
            serializer = self.serializer_class(player_data, context={"request": request})
            return Response({'status': True, 'message': 'Player Profile', 'data': serializer.data}, status=status.HTTP_200_OK)
        except Player.DoesNotExist:
            return Response({'status': False, 'message': 'Player not found'}, status=status.HTTP_400_BAD_REQUEST)


class PlayCricket(APIView):
    """
    """

    @csrf_exempt
    def post(self, request):
        team_1 = request.data.get('team_1')
        team_2 = request.data.get('team_2')

        # get team
        first_team = get_team_by_id(team_1)
        second_team = get_team_by_id(team_2)

        # for team_1 innings
        all_players_team_1 = Player.objects.filter(team__id=team_1)
        team_1_innings_result = list(map(play_innings, all_players_team_1))
        # for team_2 innings
        all_players_team_2 = Player.objects.filter(team__id=team_2)
        team_2_innings_result = list(map(play_innings, all_players_team_2))
        # result
        if sum(team_1_innings_result) > sum(team_2_innings_result):
            Matches.objects.create(team_1=first_team,
                                   team_2=second_team,
                                   winner=first_team)
            update_or_create_point_table(first_team, 2)
            return Response({
                'status': True,
                'message': 'Team {0} is winner'.format(first_team.name),
                'data': {
                    'team_1': first_team.name,
                    'team_2': second_team.name,
                    'winner': first_team.name,
                    'team_1_run': sum(team_1_innings_result),
                    'team_2_run': sum(team_2_innings_result),
                    'win_by': sum(team_1_innings_result) - sum(team_2_innings_result)
                }
            }, status=status.HTTP_200_OK)
        elif sum(team_1_innings_result) < sum(team_2_innings_result):
            Matches.objects.create(team_1=first_team,
                                   team_2=second_team,
                                   winner=second_team)
            update_or_create_point_table(second_team, 2)
            return Response({
                'status': True,
                'message': 'Team {0} is winner'.format(second_team.name),
                'data': {
                    'team_1': first_team.name,
                    'team_2': second_team.name,
                    'winner': second_team.name,
                    'team_1_run': sum(team_1_innings_result),
                    'team_2_run': sum(team_2_innings_result),
                    'win_by': sum(team_2_innings_result) - sum(team_1_innings_result)
                }
            }, status=status.HTTP_200_OK)
        elif sum(team_1_innings_result) == sum(team_2_innings_result):
            Matches.objects.create(team_1=first_team,
                                   team_2=second_team)
            update_or_create_point_table(first_team, 1)
            update_or_create_point_table(second_team, 1)
            return Response({
                'status': True,
                'message': 'Match draw',
                'data': {
                    'team_1': first_team.name,
                    'team_2': second_team.name,
                    'winner': 'Draw',
                    'team_1_run': sum(team_1_innings_result),
                    'team_2_run': sum(team_2_innings_result),
                    'win_by': 0
                }
            }, status=status.HTTP_200_OK)


def play_innings(player):
    run = random.randrange(0, 100)
    try:
        old_history = PlayerHistory.objects.get(player=player)
        try:
            history = PlayerHistory.objects.get(player=player)
            history.matches = old_history.matches + 1
            history.run = old_history.run + run
            if run > old_history.highest_scores:
                history.highest_scores = run
            if run >= 100:
                history.hundreds = old_history.hundreds + 1
            elif 50 <= run < 100:
                history.fifties = old_history.fifties + 1
            history.save()
        except Exception as e:
            print(e)
            return {player, 0}
    except PlayerHistory.DoesNotExist:
        try:
            history = PlayerHistory.objects.create(player=player)
            history.matches = 1
            history.run = run
            history.highest_scores = run
            if run >= 100:
                history.hundreds = 1
            elif 50 <= run < 100:
                history.fifties = 1
            history.save()
        except Exception as e:
            print(e)
            return {player, 0}
    return run


def update_or_create_point_table(team, point):
    try:
        old_point_table = PointTable.objects.get(team=team)
        old_point_table.point = old_point_table.point + point
        old_point_table.save()
    except PointTable.DoesNotExist:
        PointTable.objects.create(team=team, point=point)


class GetPointTable(generics.ListAPIView):
    queryset = PointTable.objects.all()
    serializer_class = PointTableSerializer
